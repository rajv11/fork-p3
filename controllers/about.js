const express = require('express')
const api = express.Router()

// Specify the handler for each required combination of URI and HTTP verb

// HANDLE VIEW DISPLAY REQUESTS --------------------------------------------

// GET t1
api.get('/t3/a', (req, res) => {
  console.log(`Handling GET ${req}`)
  res.render('about/t3/a/index.ejs',
        { title: ' Dodla Shivani Reddy', layout: 'layout.ejs' })
})
api.get('/t3/b', (req, res) => {
  console.log(`Handling GET ${req}`)
  return res.render('about/t3/b/index.ejs',
        { title: 'Vamshi Raj Jennaikode', layout: 'layout.ejs' })
})
api.get('/t3/b/skills', (req, res) => {
  console.log(`Handling GET ${req}`)
  return res.render('about/t3/b/skills.ejs',
        { title: 'Vamshi Raj Jennaikode', layout: 'layout.ejs' })
})
api.get('/t3/b/contact', (req, res) => {
  console.log(`Handling GET ${req}`)
  return res.render('about/t3/b/contact.ejs',
        { title: 'Vamshi Raj Jennaikode', layout: 'layout.ejs' })
})
api.get('/t3/c', (req, res) => {
  console.log(`Handling GET ${req}`)
  return res.render('about/t3/c/index.ejs',
        { title: 'Manogna Sivangula', layout: 'layout.ejs' })
})


module.exports = api
